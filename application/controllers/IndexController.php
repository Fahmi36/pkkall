<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class IndexController extends CI_Controller {

	public function index()
	{
		$data['title'] = 'Selamat Datang Di Halaman Login PKK';
		$data['link_view'] = 'login';
		$data['show_pg_head'] = 'none';
		$this->load->view('utama',$data);
	}

	public function register()
	{
		$data['title'] = 'Selamat Datang Di Halaman Login PKK';
		$data['link_view'] = 'izin_iumk/permohonan_baru_iumk';
		$data['show_pg_head'] = 'none';
		$this->load->view('utama',$data);
	}
}

/* End of file IndexController.php */
/* Location: ./application/controllers/IndexController.php */