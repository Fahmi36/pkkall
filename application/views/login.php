
			\<div class="container">
				<div class="col-md-4 ml-auto mr-auto">
					<form class="form" method="post" action="<?=site_url('login')?>">

						<div class="card card-login card-plain">
							<div class="card-header ">
								<div class="logo-container">
									<img src="<?=base_url('assets/')?>img/now-logo.png" alt="">
								</div>
							</div>
							<div class="card-body ">
								<div class="input-group no-border form-control-lg">
									<span class="input-group-prepend">
										<div class="input-group-text">
											<i class="now-ui-icons users_circle-08"></i>
										</div>
									</span>
									<input type="text" name="username" class="form-control" placeholder="Masukan No Anggota / Username" autocomplete="off">
								</div>
								<div class="input-group no-border form-control-lg">
									<div class="input-group-prepend">
										<div class="input-group-text">
											<i class="now-ui-icons text_caps-small"></i>
										</div>
									</div>
									<input type="password" placeholder="Masukan Password" name="password" class="form-control" autocomplete="off">
								</div>
							</div>
							<div class="card-footer ">
								<button type="submit" class="btn btn-primary btn-round btn-lg btn-block mb-3">Masuk</button>
								<div class="pull-left">
									<h6><a href="#pablo" class="link footer-link">Daftar</a></h6>
								</div>
								<div class="pull-right">
									<h6><a href="#pablo" class="link footer-link">Lupa Password?</a></h6>
								</div>
							</div>
						</div>							
					</form>
				</div>
			</div>
		</div>
		