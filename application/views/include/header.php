<html lang="en">
<head>	
	<meta charset="utf-8" />
	<link rel="apple-touch-icon" sizes="76x76" href="<?=base_url('assets/')?>img/apple-icon.png">
	<link rel="icon" type="image/png" href="<?=base_url('assets/')?>img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title>Koperasi PKK Melati Jaya</title>
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />

	<!-- Extra details for Live View on GitHub Pages -->
	<!-- Canonical SEO -->
	<link rel="canonical" href="https://www.creative-tim.com/product/now-ui-dashboard-pro" />

	<!--  Social tags      -->
	<meta name="keywords" content="creative tim, html dashboard, html css dashboard, web dashboard, bootstrap 4 dashboard, bootstrap 4, css3 dashboard, bootstrap 4 admin, now ui dashboard bootstrap 4 dashboard, frontend, responsive bootstrap 4 dashboard, now ui design, now ui dashboard bootstrap 4 dashboard">
	<meta name="description" content="Now UI Dashboard PRO is a beautiful Bootstrap 4 admin dashboard with a large number of components, designed to look beautiful, clean and organized. If you are looking for a tool to manage dates about your business, this dashboard is the thing for you.">

	<!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Now Ui Dashboard PRO by Creative Tim">
	<meta itemprop="description" content="Now UI Dashboard PRO is a beautiful Bootstrap 4 admin dashboard with a large number of components, designed to look beautiful, clean and organized. If you are looking for a tool to manage dates about your business, this dashboard is the thing for you.">
	<meta itemprop="image" content="https://s3.amazonaws.com/creativetim_bucket/products/72/opt_nudp_thumbnail.jpg">
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="product">
	<meta name="twitter:site" content="@creativetim">
	<meta name="twitter:title" content="Now Ui Dashboard PRO by Creative Tim">
	<meta name="twitter:description" content="Now UI Dashboard PRO is a beautiful Bootstrap 4 admin dashboard with a large number of components, designed to look beautiful, clean and organized. If you are looking for a tool to manage dates about your business, this dashboard is the thing for you.">
	<meta name="twitter:creator" content="@creativetim">
	<meta name="twitter:image" content="https://s3.amazonaws.com/creativetim_bucket/products/72/opt_nudp_thumbnail.jpg">
	<!-- Open Graph data -->
	<meta property="fb:app_id" content="655968634437471">
	<meta property="og:title" content="Now Ui Dashboard PRO by Creative Tim" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://demos.creative-tim.com/now-ui-dashboard-pro/examples/dashboard.html" />
	<meta property="og:image" content="https://s3.amazonaws.com/creativetim_bucket/products/72/opt_nudp_thumbnail.jpg"/>
	<meta property="og:description" content="Now UI Dashboard PRO is a beautiful Bootstrap 4 admin dashboard with a large number of components, designed to look beautiful, clean and organized. If you are looking for a tool to manage dates about your business, this dashboard is the thing for you." />
	<meta property="og:site_name" content="Creative Tim" />	<!--     Fonts and icons     -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.1/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<!-- CSS Files -->
	<link href="<?=base_url('assets/')?>css/bootstrap.min.css" rel="stylesheet" />	<link href="<?=base_url('assets/')?>css/now-ui-dashboard.min.css?v=1.5.0" rel="stylesheet" />	<!-- CSS Just for demo purpose, don't include it in your project -->
	<link href="<?=base_url('assets/')?>demo/demo.css" rel="stylesheet" />
</head>

<body class="sidebar-mini ">